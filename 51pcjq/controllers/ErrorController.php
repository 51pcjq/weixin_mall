<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\log\FileTarget;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class ErrorController extends Controller
{
    public function actionError(){
        $error = \Yii::$app->errorHandler->exception;
        $err_msg="";
        if($error){
            $file=$error->getFile();
            $line=$error->getLine();
            $message=$error->getMessage();
            $code=$error->getCode();
            $log= new FileTarget();
            $log->logFile =\Yii::$app->getRuntimePath()."/logs/err.log";
            $err_msg = $message."   [file:{$file}][line:{$line}][code:{$code}][url:{$_SERVER['REQUEST_URI']}][POST_DATA:".http_build_query($_POST)."]";
            $log->messages[]=[
                $err_msg,
                1,
                "application",
                microtime(true)
            ];
            $log->export();
            //tudo写入到数据库

        }
     // $this->layout = false;
    return $this->render("error",["err_msg"=> $err_msg]);

    }
}