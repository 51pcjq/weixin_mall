<?php

namespace app\module\m\controllers;

use yii\web\Controller;

class PayController extends Controller
{
    public function actionBuy()
    {
        $this->layout=false;
        return $this->render('buy');
    }
}
