<?php

namespace app\module\web\controllers;

use yii\web\Controller;

class AccountController extends Controller
{
    public function __construct($id,$module,array $config=[]){
            parent::__construct($id,$module,$config);
            $this->layout="main";
    }
    public function actionIndex()
    {
       // $this->layout="main";
        return $this->render('index');
    }
    public function actionSet()
    {
       // $this->layout="main";
        return $this->render('set');
    }
  public function actionInfo()
    {
       // $this->layout="main";
        return $this->render('info');
    }
}
