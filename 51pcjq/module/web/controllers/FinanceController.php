<?php

namespace app\module\web\controllers;

use yii\web\Controller;

class FinanceController extends Controller
{

    public function actionIndex()
    {
        $this->layout=false;
        return $this->render("index");
    }
     public function actionAccount()
    {
        $this->layout=false;
        return $this->render('account');
    }
    public function actionPay_info()
    {
        $this->layout=false;
        return $this->render('pay_info');
    }
}
